FROM ubuntu:22.04

# hadolint ignore=DL3008
RUN apt-get update -y \
    && apt-get install -y --no-install-recommends \
        inkscape \
    && rm -rf /var/lib/apt/list/*

CMD ["/bin/bash"]
